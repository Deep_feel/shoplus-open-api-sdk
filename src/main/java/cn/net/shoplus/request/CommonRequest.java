package cn.net.shoplus.request;

import cn.net.shoplus.response.CommonResponse;

/**
 * @author tanghc
 */
public class CommonRequest extends BaseRequest<CommonResponse> {

    public CommonRequest() {
    }

    public CommonRequest(String name, String version) {
        this.setName(name);
        this.setVersion(version);
    }

    public CommonRequest(String name) {
        this.setName(name);
    }

    @Override
    public String name() {
        return "";
    }
}
