package cn.net.shoplus.request;

import cn.net.shoplus.response.FileUploadResponse;

public class FileUploadRequest extends BaseRequest<FileUploadResponse> {
    @Override
    public String name() {
        return "file.upload";
    }
}
