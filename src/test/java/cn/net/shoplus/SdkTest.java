package cn.net.shoplus;

import cn.net.shoplus.client.OpenHttp;
import cn.net.shoplus.common.OpenConfig;
import cn.net.shoplus.request.CommonRequest;
import cn.net.shoplus.response.CommonResponse;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class SdkTest extends BaseTest {

    OpenHttp openHttp = new OpenHttp(new OpenConfig());

    // 懒人版，如果不想添加Request,Response,Param。可以用这种方式，返回全部是String，后续自己处理json
    @Test
    public void testLazy() {
        // 创建请求对象
        CommonRequest request = new CommonRequest("goods.get");
        // 请求参数
        Map<String, Object> param = new HashMap<>();
        param.put("goods_name", "iphone6");
        request.setParam(param);

        // 发送请求
        CommonResponse response = client.execute(request);

        System.out.println("--------------------");
        if (response.isSuccess()) {
            // 返回结果
            Map<String, Object> goods = response.getData();
            System.out.println(goods.get("goods_name"));
        } else {
            System.out.println("errorCode:" + response.getCode() + ",errorMsg:" + response.getMsg());
        }
        System.out.println("--------------------");
    }

}
