package cn.net.shoplus;

import cn.net.shoplus.client.OpenClient;
import junit.framework.TestCase;

public class BaseTest extends TestCase {
//    static String url = "http://127.0.0.1:8066/open-api";
    static String url = "https://dev-api-portal-shoplus.algobuy.net/open-api";
    static String appKey = "shoplus";
    static String secret = "123456";

    static OpenClient client = new OpenClient(url, appKey, secret);
}
