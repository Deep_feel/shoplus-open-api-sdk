# shoplus-open-api-sdk

sdk for Java

Shoplus开放平台对应的sdk


## 接口信息

比如获取商品详情接口

- 接口名：products.detail
- 版本号："1.0"
- 请求参数：id
- 返回信息 product对象

```
{
    "code":"0",
    "data":{
        "title":"苹果iPhoneX",
        "id":1,
        "seoUrl":"*****"
    }
}
```

## maven依赖
```java
<dependency>
    <groupId>io.gitee.deep_feel</groupId>
    <artifactId>shoplus-sdk</artifactId>
    <version>1.0.0-RELEASE</version>
</dependency>
```


## sdk使用方式

```
String url = "https://test-api-portal-shoplus.algobuy.net/open-api";
String appKey = "**********************";
String secret = "*************";

OpenClient client = new OpenClient(url, appKey, secret);

// 创建请求对象
CommonRequest request = new CommonRequest("products.detail", "1.0");
// 请求参数
Map<String, Object> param = new HashMap<>(16);
param.put("id", 510112L);
request.setParam(param);

// 发送请求
CommonResponse response = client.execute(request);

if (response.isSuccess()) {
    // 返回结果
    Map<String, Object> responseData = response.getData();
    System.out.println(responseData.get("title"));
    System.out.println(responseData.get("seoUrl"));
} else {
    System.out.println("errorCode:" + response.getCode() + ",errorMsg:" + response.getMsg());
}
```